package com.up.training.entity;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "STUDENT")
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "STUDENT_ID")
	private Long studentId;

	@Column(name = "STUDENT_CODE", length = 10, nullable = false)
	private String studentCode;
	
	@Column(name = "FIRST_NAME", length = 300, nullable = false)
	private String firstName;

	@Column(name = "LAST_NAME", length = 300, nullable = true)
	private String lastName;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "DATE_OF_BIRTH", nullable = true)
	private Date dateOfBirth;

	@Column(name = "GENDER", length = 100, nullable = true)
	private String gender;

	@Column(name = "PROVINCE", length = 500, nullable = true)
	private String province;

	@Column(name = "ADDRESS", length = 1000, nullable = true)
	private String address;

	@Column(name = "EMAIL", length = 320, nullable = false)
	private String email;

	@CreatedDate
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "CREATE_DATE", nullable = true)
	private Date createDate;

	@LastModifiedDate
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "UPDATE_DATE", nullable = true)
	private Date updateDate;

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getStudentCode() {
		return studentCode;
	}

	public void setStudentCode(String studentCode) {
		this.studentCode = studentCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}
