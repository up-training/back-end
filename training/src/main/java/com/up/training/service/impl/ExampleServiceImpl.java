package com.up.training.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.up.training.model.ProfileRequest;
import com.up.training.service.ExampleService;

@Service
public class ExampleServiceImpl implements ExampleService{
	
	private List<Map<String, Object>> profiles = new ArrayList<>();
	public Integer id = 0;
	
	@Override
	public ResponseEntity<List<Map<String, Object>>> search() {
		return ResponseEntity.ok(profiles);
	}

	@Override
	public ResponseEntity<Map<String, Object>> save(ProfileRequest profileRequest){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id++);
		map.put("name", profileRequest.getName());
		map.put("age", profileRequest.getAge());
		map.put("gender", profileRequest.getGender());
		profiles.add(map);
		return ResponseEntity.ok(map);
	}

	@Override
	public ResponseEntity<?> delete(Integer id) {
		for (Map<String, Object> map : profiles) {
			if(map.get("id") == id) {
				profiles.remove(map);
				return ResponseEntity.ok(profiles);
			}
	      }  
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body("NOT_FOUND");
	}
	
	
}
