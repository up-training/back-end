package com.up.training.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.up.training.model.ProfileRequest;

public interface ExampleService {
	ResponseEntity<List<Map<String, Object>>>  search();
	
	ResponseEntity<Map<String, Object>> save(ProfileRequest profileRequest);
	
	ResponseEntity<?> delete(Integer id);
}
