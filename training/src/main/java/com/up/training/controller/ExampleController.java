package com.up.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.up.training.model.ProfileRequest;
import com.up.training.service.ExampleService;

@RestController
@RequestMapping("/example")
public class ExampleController {
	
	@Autowired
	private ExampleService exampleService;
	
	@GetMapping("/hello")
	public ResponseEntity<?> hello() {
		return ResponseEntity.ok("Hello");
	}

	@GetMapping("/helloName")
	public ResponseEntity<?> helloName(@RequestParam String name) {
		return ResponseEntity.ok("Hello " + name);
	}
	
	@GetMapping("/searchProfile")
	public ResponseEntity<?> searchProfile() {
		return exampleService.search();
	}
	
	@PostMapping("/saveProfile")
	public ResponseEntity<?> saveProfile(@RequestBody ProfileRequest profileRequest) {
		return exampleService.save(profileRequest);
	}
	
	@GetMapping("/deleteProfile")
	public ResponseEntity<?> deleteProfile(@RequestParam Integer id) {
		return exampleService.delete(id);
	}

}
