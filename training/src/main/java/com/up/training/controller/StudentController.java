package com.up.training.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.up.training.entity.Student;
import com.up.training.repository.StudentRepository;

@RestController
@RequestMapping("/student")
public class StudentController {
	
	@Autowired
	private StudentRepository studentRepository;
	
	@GetMapping("/search")
	public ResponseEntity<?> search() {
		List<Student> students = studentRepository.findAll();
		return ResponseEntity.ok(students);
	}
	
	@PostMapping("/save")
	public ResponseEntity<?> save(@RequestBody Student student) {
		studentRepository.save(student);
		return ResponseEntity.ok(student);
	}
	
	@GetMapping("/delete")
	public ResponseEntity<?> delete(@RequestParam Long studentId) {
		studentRepository.deleteById(studentId);
		return ResponseEntity.ok(true);
	}

}
