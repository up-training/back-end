package com.up.training.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.up.training.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {

}
